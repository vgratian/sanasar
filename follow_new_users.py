import twitter
from config import *
from datetime import date, datetime
from time import sleep, strptime

"""
This module works as a poller: finds new users that Tweet in Armenian and follows them.
This is done by periodically searching for most frequent terms used in Armenian tweets,
checking if the authors of these tweets meet certain requirements (see below) and if
yes by following them.

The criteria for following a user are:
    - total number of tweets
    - average time between tweets
    - percentage tweets with URL
    - percentage tweets in target language

For the specific parameters of these criteria, see under def check_user().
"""

global followed_users, freq_words, api, lang

followed_users = set()
lang = 'hy'
# most frequently used words in tweets in target language
freq_words = ['եմ', 'ա', 'ես', 'որ', 'ու', 'էլ', 'մի', 'են', 'դ']
api = twitter.Api(  consumer_key,
                    consumer_secret,
                    access_token_key,
                    access_token_secret,
                    tweet_mode='extended')


def run():
    global followed_users

    print('[NORMAL] Starting session at {}'.format(datetime.utcnow()))

    # Get list of users who are already followed
    try:
        followed_users = get_friends()
    except:
        print('[ERROR] Unable to fetch list of followed users')
    else:
        print('[NORMAL] Retrieved list of {} followed users'.format(len(followed_users)))

    # Search for new users every 30 minutes
    while(True):
        try:
            # Recently active users
            recently_active = get_active_users()
            print('{} [NORMAL] Restarting session at {}{}'.format('\033[1m',datetime.utcnow(),'\033[0m'))
        except:
            print('[ERROR] Unable to fetch list of recently active users.')

        else:
            # Follow users that meet criteria
            print('[NORMAL] Checking {} recently active users' \
                .format(len(recently_active)))
            for user in recently_active:
                if check_user(user):
                    username = follow_user(user)
                    if username:
                        print('{}[UPDATE] Following new user: @{}{}'.format('\033[1m',username,'\033[0m'))
                sleep(0.2)
        finally:
            print('[NORMAL] Sleeping for 20 minutes.')
            sleep(1200)


def get_friends():
    """
    Returns list of users that are followed by the bot
    """
    return set(f.id for f in api.GetFriends())


def get_active_users():
    """
    Get's a list of users that recently tweeted
    """
    active_users = []

    for word in freq_words:
        results = api.GetSearch(term=word, result_type='recent', count=100)
        for tweet in results:
            user = tweet.user.id
            if user not in followed_users and user not in active_users:
                active_users.append(user)

    return active_users


def check_user(user):
    """
    Checks if user meets the criteria to be followed. Return value is boolean.

    Criteria:
        - Total number of tweets (>= 10)
        - Average time between tweets (<= 5 days)
        - Percentage tweets with URLs (<= 40%)
        - Percentage tweets in target language (>= 50%)
    """
    contains_url = 0.0
    in_targ_lang = 0.0
    tweets_age = 0.0
    count = 0

    last_tweets = api.GetUserTimeline(user_id=user, count=25)

    for tweet in last_tweets:
        count += 1
        tweets_age -= get_tweet_age(tweet.created_at)
        if tweet.lang == lang:
            in_targ_lang += 1
        if tweet.urls:
            contains_url += 1

    # Normalize by Tweet count
    contains_url /= count
    in_targ_lang /= count
    tweets_age /= count

    good_user = True

    # Cases to ignore user

    # New user or rarely active user
    if count < 10 or tweets_age > 5:
        good_user = False
    # Most tweets not in target language  or many urls, i.e. not authentic tweets
    elif contains_url > 0.4 or in_targ_lang < 0.5:
        good_user = False

    return good_user


def get_tweet_age(created_at):
    """
    Returns tweet age in days
    """
    now = datetime.utcnow()
    current = date(now.year, now.month, now.day)
    created = format_date(created_at)
    delta = current - created

    return delta.days


def format_date(long_date):
    """
    Returns a date object based on the datastamp provided.
    Input is a string that comes either from Twitter API or time.ctime(), e.g.:
    "Fri Jul 13 11:28:23 +0000 2018"
    "Sat Jul 14 00:58:19 2018"
    """
    list_date = long_date.split()

    year = int(list_date[-1])
    month = strptime(list_date[1], '%b').tm_mon
    day = int(list_date[2])

    return date(year, month, day)


def follow_user(user):
    """
    Follows the given user on Tweeter. Input is user ID, return screen
    name of the user (string).
    """
    global followed_users
    try:
        response = api.CreateFriendship(user_id=user)
    except:
        print('[WARNING] Unable to follow user with ID {}'.format(user))
        return None
    else:
        followed_users.add(response.id)
        return response.screen_name


if __name__ == '__main__':
    run()
