import twitter, time
from random import randint
from config import *

global last_id, api
last_id = 945072842504392707 
last_msg_id = 944808084597362693 
api = twitter.Api(  consumer_key,
                    consumer_secret,
                    access_token_key,
                    access_token_secret,
                    tweet_mode='extended')


def run():
    global last_id, last_msg_id

    while(True):
        try:
            feed = get_mentions()
        except:
            pass
        else:
            if feed:
                last_id_updated = False
                for mention in feed:
                    print('New mention: ', mention.full_text)
                    if not last_id_updated:
                        last_id = mention.id
                        last_id_updated = True
                    reply = read_mention(mention.full_text)
                    if reply:
                        print('Posting reply: ', reply)
                        post_reply(reply, mention.id, mention.user.screen_name)
        finally:
            time.sleep(35)

        try:
            messages = get_messages()
        except:
            pass
        else:
            if messages:
                last_id_updated = False
                for msg in messages:
                    print('New direct message: ', msg.text)
                    if not last_id_updated:
                        last_msg_id = msg.id
                        last_id_updated = True
                    reply = read_mention(msg.text)
                    if reply:
                        send_reply(reply, msg.sender_screen_name)
                    else:
                        send_reply('անծանոթ բառ էր', msg.sender_screen_name)
        finally:
            time.sleep(45)


def get_mentions():
    global last_id, api

    try:
        feed = api.GetMentions(count=10, since_id=last_id)
    except:
        print('Failed to retrieve mentions.')
        return None
    else:
        if feed:
            print('Fetching {} mentions...'.format(len(feed)))
            for mention in feed:
                yield mention


def get_messages():
    global last_msg_id, api

    try:
        messages = api.GetDirectMessages(count=20, since_id=last_msg_id)
    except:
        print('Failed to retrieve messages.')
        return None
    else:
        if messages:
            print('Fetching {} messages...'.format(len(messages)))
            for msg in messages:
                yield msg


def post_reply(reply, tw_id, user):
    url = 'https://twitter.com/{}/status/{}'.format(user, tw_id)
    try:
        status = api.PostUpdate(status=reply, in_reply_to_status_id=tw_id, attachment_url=url)
    except:
        print('Failed to post reply.')


def send_reply(reply, user_name):
    try:
        status = api.PostDirectMessage(text=reply, screen_name=user_name)
    except:
        print('Failed to send message.')


def read_mention(tweet):
    tweet = tweet.lower().split()
    tokens = []
    for word in tweet:
        if word[0] != '@':
            tokens.append(word)

    if tokens:
        if len(tokens) > 1:
            return None
        else:
            reply = compose_sentence(tokens[0])
            return reply


def compose_sentence(word):
    sentence = [word]
    next_word = word
    while next_word[-1] != '։':
        next_word = get_matching_bigram(next_word, len(sentence))
        next_word = next_word.split()[1]
        sentence.append(next_word)
        if len(sentence) > 10:
            break

    if len(sentence) < 3:
        return None

    sentence = ' '.join(sentence[:-1]) + '։' # no space before period
    if sentence[-3:] == ' ճ։':
        sentence = sentence [:-3] + ' :Ճ'
    elif sentence[-3:] == ' դ։':
        sentence = sentence [:-3] + ' :Դ'
    return sentence


def get_matching_bigram(word, size):
    """
    Given an input word (n) returns a bigram (n, n-1) with the highest probability
    """
    # In order to calculate bigram probability we first need word frequency, so:
    # Get word frequency from data
    word_freq = get_word_freq(word)

    # Read all bigrams beginning word and calculate probablities
    bigrams = {}
    bigrams_data = read_bigrams(word)

    for bigram in bigrams_data:
        bigram, freq = bigram.split('\t')
        bigrams[bigram] = int(freq)

    # Calculate bigram probability based on bigram and unigram frequencies
    for bigram in bigrams:
        bigrams[bigram] = bigrams[bigram] / word_freq

    sorted_bigrams = sorted(bigrams.items(), key=lambda x:x[1], reverse=True)[:3]

    # Decide what to return

    # when no matching bigrams, return word as last part of sentence
    if len(sorted_bigrams) == 0:
        return '{} ։'.format(word)

    # if sentence is less than 3 words, avoid returning period
    if size < 3 and len(sorted_bigrams) > 1:
        if '։' in sorted_bigrams[0][0]:
            return sorted_bigrams[1][0]

    # if bigram is same word, return something else (or we'll get infinite loop)
    if sorted_bigrams[0][0].split()[1] == word:
        if len(sorted_bigrams) > 1:
            return sorted_bigrams[1][0]
        else:
            return '{} ։'.format(word)

    # standard case
    return sorted_bigrams[0][0]


def read_bigrams(word):

    # Define filename / filepath for bigram
    first_letter = word[0]
    if first_letter not in hyalpha:
        first_letter = '_'
    fp = 'data/bigrams/{}.txt'.format(first_letter)

    # Read from files
    try:
        f = open(fp, 'r')
    except:
        print('Can\'t read bigram file')
        raise IOError
    else:
        for bigram in f.read().splitlines():
            if word == bigram.split()[0]:
                yield bigram
    finally:
        f.close()


def get_word_freq(word):

    word_freq = 0

    # Define filename / filepath for unigram
    first_letter = word[0]
    if first_letter not in hyalpha:
        first_letter = '_'
    fp = 'data/unigrams/{}.txt'.format(first_letter)

    # Open unigram file and get frequency
    with open(fp, 'r') as unigrams:
        for unigram in unigrams.read().splitlines():
            if word == unigram.split()[0]:
                word_freq = int(unigram.split()[1])
                break

    return word_freq


if __name__ == '__main__':
    run()

